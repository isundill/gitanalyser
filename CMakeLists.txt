cmake_minimum_required(VERSION 2.8)

set(CXX_COMPILER_ID,Clang)
set(CMAKE_CXX_FLAGS "-std=c++11 -W -Wall -Wextra -pedantic -g3")
project(gitAnalyser)
include_directories(include/)
	add_custom_command(
			OUTPUT Chart.min.js.o
			COMMAND ld -r -b binary Chart.min.js -o Chart.min.js.o
			)
aux_source_directory(src/ src)
add_executable(gitAnalyser ${src} Chart.min.js.o)

