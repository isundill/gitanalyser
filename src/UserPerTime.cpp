#include "GitLog.hpp"
#include "UserPerTime.hpp"

UserPerTime::UserPerTime(int f, int t): from(f), to(t)
{ }

StatMaker::statEntry UserPerTime::compute(const GitLog &log, const std::list<GitEntity*>&) const
{
	const GitLog::LogMap &map = log.getMap();
	StatMaker::statEntry result;

	for (auto i = map.cbegin(); i != map.cend(); i++)
	{
		GitLog::LogEntry *entry = (*i).second;

		if ((from < to && entry->time.tm_hour > from && entry->time.tm_hour < to) ||
			(from > to && (entry->time.tm_hour > from || entry->time.tm_hour < to)))
			result[entry->email]++;
	}
	return result;
}

