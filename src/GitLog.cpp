#include <fstream>
#include <iostream>
#include <sstream>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "GitLog.hpp"

GitLog::GitLog(const std::string &rev)
{
	int fd_pipes[2];
	int fk;

	if (pipe(fd_pipes) == -1)
		throw std::runtime_error("Cannot read from process");
	if ((fk = fork()) == -1)
		throw std::runtime_error("Cannot fork");
	if (fk == 0)
	{
		int devnull = open("/dev/null", O_WRONLY);
		close(fd_pipes[0]);
		dup2(fd_pipes[1], 1);
		dup2(devnull, 2);
		execlp("git", "git", "log", "--pretty=%H %ae %ct %S", rev.c_str(), nullptr);
		std::cerr << strerror(errno) << std::endl;
		exit(EXIT_SUCCESS);
	}
	else
	{
		int s;
		FILE *f;
		char *line = nullptr;
		size_t len = 0;

		close(fd_pipes[1]);
		f = fdopen(fd_pipes[0], "r");
		while (getline(&line, &len, f) != -1)
			if (line)
			{
				std::string l (line);
				std::string hash;

				l.pop_back();
				parseLine(line);
				free(line);
				line = nullptr;
				len = 0;
			}
		if (line)
			free(line);
		::wait(&s);
		fclose(f);
	}
}

GitLog::~GitLog()
{
	for (auto i = map.cbegin(); i != map.cend(); i++)
		delete (*i).second;
}

void GitLog::parseLine(const std::string &line)
{
	std::string hash;
	std::stringstream ss (line);
	GitLog::LogEntry *lEntry;
	size_t timestamp;

	lEntry = new GitLog::LogEntry();
	lEntry->usefull =0;
	lEntry->totalChar =0;
	ss >> hash >> lEntry->email >> timestamp;
	std::getline(ss, lEntry->message);
	gmtime_r((time_t*)&timestamp, &(lEntry->time));
	map[hash] = lEntry;
}

static inline bool hash_cmp(const char *a, const char *b)
{
	if (*a == *b && *a)
		return hash_cmp(a+1, b+1);
	else if (!(*a) || !(*b))
		return true;
	return false;
}

GitLog::LogEntry *GitLog::getEntry(const std::string &h)
{
	std::string hash = h;
	if (hash.at(0) == '^')
		hash = hash.substr(1);
	for (auto i = map.cbegin(); i != map.cend(); i++)
	{
		if (hash_cmp(hash.c_str(), (*i).first.c_str()))
			return (*i).second;
	}
	return nullptr;
}

const GitLog::LogMap &GitLog::getMap() const
{ return map; }

std::string GitLog::LogEntry::toString(const std::string &hash) const
{
	return hash.substr(0, 10) +"\t" +asctime(&this->time) +"\t" +this->email;
}

