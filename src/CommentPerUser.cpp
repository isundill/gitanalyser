#include "GitFile.hpp"
#include "StatCommentPerUser.hpp"
#include "GitLog.hpp"

StatMaker::statEntry CommentPerUser::compute(const GitLog &a, const std::list<GitEntity *> &l) const
{
	StatMaker::statEntry result;
	for (auto i = l.cbegin(); i != l.cend(); i++)
	{
		const GitEntity &log = **i;
		const GitDir *dir = dynamic_cast<const GitDir *> (&log);
		const GitFile *file = dynamic_cast<const GitFile *> (&log);

		if (file)
			for (auto i = file->getLines().cbegin(); i != file->getLines().cend(); i++)
			{
				const GitLog::LogEntry *entry = (*i)->getEntry();
				if (!entry)
					continue;
				if ((*i)->haveComment())
					result[entry->email]++;
			}
		else if (dir)
		{
			StatMaker::statEntry c = this->compute(a, dir->getEntities());
			result = StatMaker::merge(result, c);
		}
	}

	return result;
}

void CommentPerLine::doCompute(const std::list<GitEntity *> &root, std::map<std::string, unsigned long> &com, std::map<std::string, unsigned long> &line) const
{
	for (auto i = root.cbegin(); i != root.cend(); i++)
	{
		const GitEntity &log = **i;
		const GitDir *dir = dynamic_cast<const GitDir *> (&log);
		const GitFile *file = dynamic_cast<const GitFile *> (&log);

		if (file)
			for (auto i = file->getLines().cbegin(); i != file->getLines().cend(); i++)
			{
				const GitLog::LogEntry *entry = (*i)->getEntry();
				if (!entry)
					continue;
				line[entry->email]++;
				if ((*i)->haveComment())
					com[entry->email]++;
			}
		else if (dir)
			this->doCompute(dir->getEntities(), com, line);
	}
}

StatMaker::statEntry CommentPerLine::compute(const GitLog &, const std::list<GitEntity *> &l) const
{
	StatMaker::statEntry result;
	std::map<std::string, unsigned long int> totalLine;
	std::map<std::string, unsigned long int> totalComment;

	this->doCompute(l, totalComment, totalLine);
	for (auto i = totalLine.cbegin(); i != totalLine.cend(); i++)
	{
		std::string email = (*i).first;
		if ((*i).second == 0)
		{
			result[email] = 0;
			continue;
		}
		result[email] = (double)(totalComment[email]) / (double)((*i).second);
	}
	return result;
}

