#include "GitFile.hpp"
#include "StatLineLen.hpp"
#include "GitLog.hpp"
#include "StatLinePerUser.hpp"

StatMaker::statEntry CharPerLine::compute(const GitLog &a, const std::list<GitEntity*>&b) const
{
	StatMaker::statEntry result;
	StatMaker::statEntry nbLines;
	LinePerUser lpu;
	std::list<std::string> toRemove;
	const GitLog::LogMap &logs = a.getMap();

	for (auto _l = logs.cbegin(); _l != logs.cend(); _l++)
	{
		GitLog::LogEntry *l = (*_l).second;
		result[l->email] += l->totalChar;
	}
	nbLines = lpu.compute(a, b);
	for (auto i = result.cbegin(); i != result.cend(); i++)
	{
		size_t nbLineForUser =0;

		try
		{
			nbLineForUser = nbLines.at((*i).first);
		}
		catch (std::exception &e)
		{
			toRemove.push_back((*i).first);
			continue;
		}
		result[(*i).first] /= nbLineForUser;
	}
	for (auto i = toRemove.cbegin(); i != toRemove.cend(); i++)
		result.erase(*i);
	return result;
}

