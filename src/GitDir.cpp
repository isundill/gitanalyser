#include <iostream>
#include <stdexcept>

#include <sys/types.h>
#include <dirent.h>

#include "GitFile.hpp"
#include "ProgParams.hpp"

GitDir::GitDir(const std::string &path, const Params &param): GitEntity(path)
{
	DIR * d;
	struct dirent *dirContent;

	d = opendir(path.c_str());
	while ((dirContent = readdir(d)) != nullptr)
	{
		std::string filename = std::string(dirContent->d_name);
		GitEntity *i;

		if (filename == "." || filename == ".." || filename == ".git")
			continue;
		if (param.isExcluded(filename))
			continue;
		try {
			i = GitEntity::create(path +"/" +filename, param);
		}
		catch (std::runtime_error &e)
		{ continue; }
		if (i != nullptr)
			_content.push_back(i);
	}
	closedir(d);
}

GitDir::~GitDir ()
{
	for (auto i = _content.cbegin(); i != _content.cend(); i++)
		delete *i;
}

void GitDir::debug(size_t indentLevel) const
{
	for (size_t i =0; i < indentLevel; i++)
		std::cout << " ";
	std::cout << _filename << std::endl;
	for (auto i = _content.cbegin(); i != _content.cend(); i++)
	{
		GitEntity *_i = *i;
		_i->debug(indentLevel +1);
	}
}

void GitDir::doMagic(const Params &p, GitLog &e)
{
	for (auto i = _content.begin(); i != _content.end(); i++)
		(*i)->doMagic(p, e);
}

const std::list<GitEntity *> &GitDir::getEntities() const
{ return _content; }

