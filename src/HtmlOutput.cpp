#include <iostream>
#include <stdexcept>
#include <sstream>

#include "HtmlOutput.hpp"

extern char _binary_Chart_min_js_start[];
extern char _binary_Chart_min_js_end  [];

HtmlOutput::HtmlOutput(const std::string &_path): path(_path)
{ }

HtmlOutput::~HtmlOutput()
{ this->close(); }

void HtmlOutput::writeJs() const
{
	std::ofstream out(path +"/Chart.min.js");
	if (!out.is_open())
	{
		std::cerr << "Error: cannot open output file for writting" << std::endl;
		throw std::runtime_error("Cannot open output");
	}
	out.write(_binary_Chart_min_js_start, _binary_Chart_min_js_end -_binary_Chart_min_js_start);
	out.close();
}

std::ofstream &HtmlOutput::operator ()()
{
	if (!htmlOut.is_open())
	{
		htmlOut.open(path +"/index.html");
		if (!htmlOut.is_open())
			throw std::runtime_error("Cannot open output");
		this->printHeader();
	}
	return htmlOut;
}

void HtmlOutput::close()
{
	if (!htmlOut.is_open())
		return;
	this->printFooter();
	htmlOut.close();
}

void HtmlOutput::startSection(const std::string &name, const std::string &type, const std::string &section)
{
	if (!this)
		return;
	endSection();
	(*this)();
	htmlOut << "<p><p>" << section << "</p><canvas id='canvas_chart_" << name << "' width='800' height='600'></canvas><script>\
var ctx_chart_" << name << "=document.getElementById('canvas_chart_" << name << "').getContext('2d');\
var chart_" << name << "=new Chart(ctx_chart_" << name << ")." << type << "({";
	currentType = type;
	currentName = name;
	firstData = true;
}

void HtmlOutput::printHeader()
{
	htmlOut << "<!DOCTYPE html><html><head><script src='Chart.min.js'></script></head><body>";
}

void HtmlOutput::printFooter()
{
	endSection();
	htmlOut << "</body></html>";
}

void HtmlOutput::endSection()
{
	if (currentType == "" || currentName == "")
		return;
	htmlOut << "});</script></p>";
	currentType = "";
	currentName = "";
}

void HtmlOutput::addDataLabel(const std::list<std::string> &labels)
{
	std::ofstream &s = ((*this)());
	bool first = true;

	s << "labels: [";
	for (std::list<std::string>::const_iterator i =labels.cbegin(); i != labels.cend(); i++)
	{
		std::string value = (*i);
		if (!first)
			s << ",";
		while (value.find("\n") != value.npos)
			value = value.erase(value.find('\n'));
		s << "\"" << value << "\"";
		first = false;
	}
	s << "],datasets:[{data:[";
}

void HtmlOutput::closeData()
{
	if (!this)
		return;
	std::ofstream &out = (*this)();
	out << "]}]";
}

template <>
void HtmlOutput::addData(const std::string &n)
{
	if (this == nullptr)
		return;
	std::string value = n;

	while (value.find("\n"))
		value = value.erase(value.find('\n'));
	if (!firstData)
		((*this)()) << ",";
	((*this)()) << value;
	firstData = false;
}

template <typename T>
void HtmlOutput::addData(const T &data)
{
	if (this == nullptr)
		return;

	if (!firstData)
		((*this)()) << ",";
	((*this)()) << data;
	firstData = false;
}

template void HtmlOutput::addData<double>(const double &);
template void HtmlOutput::addData<bool>(const bool &);

