#include <regex>
#include "ProgParams.hpp"

Params::Params()
{
	_path.push_back(".");
	_rev = "HEAD";
}

Params::Params(const std::list<std::string> &av): Params()
{
	readArgs(av);
}

Params::~Params()
{ }

void Params::readArgs(const std::list<std::string> &av)
{
	bool files = false;

	_path.clear();
	for (auto i = av.cbegin(); i != av.cend(); i++)
	{
		if (*i == "--")
			files = true;
		else if (files == false && *i == "--html")
		{
			i++;
			html_output = *i;
		}
		else if (files == false && *i == "-e")
		{
			i++;
			excludeList.push_back(*i);
		}
		else if (files == false)
		{
			_rev = *i;
			files = true;
		}
		else
			_path.push_back(*i);
	}
	if (_path.size() == 0)
		_path.push_back(".");
}

bool Params::isExcluded(const std::string &_reg, const std::string &path) const
{
	std::regex reg(_reg);
	return std::regex_match(path, reg);
}

bool Params::isExcluded(const std::string &path) const
{
	for (std::list<std::string>::const_iterator i =excludeList.cbegin(); i != excludeList.cend(); i++)
		if (isExcluded(*i, path))
			return true;
	return false;
}

bool Params::htmlOutput() const
{ return html_output != ""; }

std::string Params::getOutputFile() const
{ return html_output; }

std::string Params::rev() const
{ return _rev; }

Params::t_fileList Params::path() const
{ return _path; }

