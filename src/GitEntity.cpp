#include <iostream>
#include <stdexcept>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "GitFile.hpp"

GitEntity::GitEntity(const std::string &f): _filename(f)
{ }

GitEntity::~GitEntity()
{ }

GitEntity *GitEntity::create(const std::string &path, const Params &params)
{
	GitEntity *result = nullptr;
	struct stat fileStat;

	if ((stat(path.c_str(), &fileStat)) == -1)
		throw std::runtime_error(strerror(errno));
	if (S_ISDIR(fileStat.st_mode))
		result = new GitDir(path, params);
	else if (S_ISREG(fileStat.st_mode))
		result = new GitFile(path);
	return result;
}

void GitEntity::debug(size_t indentLevel) const
{
	for (size_t i =0; i < indentLevel; i++)
		std::cout << " ";
	std::cout << _filename << std::endl;
}

