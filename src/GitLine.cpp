#include <iostream>
#include "GitLine.hpp"

GitLine::GitLine(GitLog::LogEntry *c, size_t len): co(c), containComment(false)
{
	(c->usefull)++;
	(c->totalChar) += len;
}

bool GitLine::eval(const std::string &l, bool was, bool isPython)
{
	bool result = false;
	size_t begin;

	if (was)
		result = containComment = true;
	if ((l.find('#') != l.npos && isPython) || l.find("//") != l.npos)
		containComment = true;
	if ((begin = l.rfind("/*")) != l.npos || (begin = l.rfind("<!--")) != l.npos)
	{
		size_t end;

		containComment = true;
		result = true;
		if ((end = l.rfind("*/")) != l.npos || (end = l.rfind("-->")) != l.npos)
		{
			if (end > begin)
				result = false;
		}
	}
	else if (was)
	{
		if (l.rfind("*/") != l.npos || l.rfind("-->") != l.npos)
			result = false;
		containComment = true;
	}
	return result;
}

const GitLog::LogEntry *GitLine::getEntry() const
{ return co; }

bool GitLine::haveComment() const
{ return containComment; }

