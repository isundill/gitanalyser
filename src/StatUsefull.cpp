#include "GitFile.hpp"
#include "StatUsefull.hpp"
#include "GitLog.hpp"

StatMaker::statEntry UsefullCommit::compute(const GitLog &a, const std::list<GitEntity*>&) const
{
	StatMaker::statEntry result;
	const GitLog::LogMap &map = a.getMap();

	for (auto _a = map.cbegin(); _a != map.cend(); _a++)
		result[(*_a).second->toString((*_a).first)] = (*_a).second->usefull;
	return result;
}

