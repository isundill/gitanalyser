#include <iostream>
#include <list>
#include <string>
#include "GitFile.hpp"
#include "GitLog.hpp"
#include "ProgParams.hpp"

#include "CommitPerUser.hpp"
#include "UserPerTime.hpp"
#include "StatLinePerUser.hpp"
#include "StatCommentPerUser.hpp"
#include "StatUsefull.hpp"
#include "StatLineLen.hpp"
#include "StatFile.hpp"

#include "HtmlOutput.hpp"

std::list<std::string> argsToList(char **av)
{
	std::list<std::string> result;
	for (unsigned int i =1; av[i]; ++i)
		result.push_back(av[i]);
	return result;
}

Params *parseParams(const std::list<std::string> av)
{
	Params *result = new Params(av);
	return result;
}

int main(int, char **av)
{
	const Params * params = parseParams(argsToList(av));
	HtmlOutput *out = nullptr;

	std::list<GitEntity *> rootEntity;
	Params::t_fileList rootList = params->path();
	GitLog *log = new GitLog(params->rev());

	for (auto i = rootList.cbegin(); i != rootList.cend(); i++)
	{
		GitEntity *r = GitEntity::create(params->path().front(), *params);
		rootEntity.push_back(r);
		r->doMagic(*params, *log);
	}

	if (params->htmlOutput())
		out = new HtmlOutput(params->getOutputFile());

	CommitPerUser userPerCommit;
	UserPerTime userPerTime(6, 9);
	UserPerTime userPerTimeNight(19, 3);
	LinePerUser lineUser = LinePerUser();
	CommentPerUser commentUser;
	CommentPerLine commentPerLine;
	UsefullCommit usefullCommits;
	CharPerLine avgChar;
	StatFile fileInfo;

	std::cout << "Commit / users:" << std::endl;
	out->startSection("Commits_users", "Bar", "Commits per users");
	StatMaker::printMap(userPerCommit.compute(*log, rootEntity), out);
	std::cout << "Morning users (6AM - 9AM):" << std::endl;
	out->startSection("morning_users", "Bar", "Users who works in the morning");
	StatMaker::printMap(userPerTime.compute(*log, rootEntity), out);
	out->startSection("evening_users", "Bar", "Users who works in the evening");
	std::cout << "Evening users (7PM - 3AM):" << std::endl;
	StatMaker::printMap(userPerTimeNight.compute(*log, rootEntity), out);
	out->startSection("total_line", "Bar", "Total lines per users");
	std::cout << "Total lines:" << std::endl;
	StatMaker::printMap(lineUser.compute(*log, rootEntity), out);
	out->startSection("comments", "Bar", "Lines of comments per users");
	std::cout << "Total comments:" << std::endl;
	StatMaker::printMap(commentUser.compute(*log, rootEntity), out);
	out->startSection("ratio_com_code", "Bar", "Ratio Comment / code");
	std::cout << "Ratio comment / line:" << std::endl;
	StatMaker::printRatioMap(commentPerLine.compute(*log, rootEntity), out);
	out->startSection("usefull_com", "Bar", "The most usefull commits");
	std::cout << "Commits the most presents:" << std::endl;
	StatMaker::printMap(usefullCommits.compute(*log, rootEntity), out, 3);
	out->startSection("line", "Bar", "Average of char per lines");
	std::cout << "Average char: " << std::endl;
	StatMaker::printMap(avgChar.compute(*log, rootEntity), out);
	out->startSection("file_stats", "Bar", "Files statistics");
	std::cout << "File statistics: " << std::endl;
	StatMaker::printRatioMap(fileInfo.compute(*log, rootEntity), out);

	if (out)
	{
		out->writeJs();
		out->close();
		delete out;
	}
	delete log;
	delete params;
	for (auto i = rootEntity.cbegin(); i != rootEntity.cend(); i++)
		delete *i;

	return 0;
}

