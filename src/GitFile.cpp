#include <iostream>
#include <sstream>
#include <stdexcept>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "GitFile.hpp"
#include "ProgParams.hpp"

GitFile::GitFile(const std::string &path): GitEntity(path)
{
}

GitFile::~GitFile()
{ }

void GitFile::doMagic(const Params &p, GitLog &log)
{
	int fd_pipes[2];
	int fk;

	if (pipe(fd_pipes) == -1)
		throw std::runtime_error("Cannot read from process");
	if ((fk = fork()) == -1)
		throw std::runtime_error("Cannot fork");
	if (fk == 0)
	{
		int devnull = open("/dev/null", O_WRONLY);
		close(fd_pipes[0]);
		dup2(fd_pipes[1], 1);
		dup2(devnull, 2);
		execlp("git", "git", "blame", "-l", p.rev().c_str(), "--", this->_filename.c_str(), nullptr);
		exit(EXIT_SUCCESS);
	}
	else
	{
		int s;
		FILE *f;
		char *line = nullptr;
		size_t len = 0;
		bool wasComment = false;
		const bool isPython = this->_filename.rfind(".py") == this->_filename.size() -3;

		close(fd_pipes[1]);
		f = fdopen(fd_pipes[0], "r");
		while (getline(&line, &len, f) != -1)
			if (line)
			{
				std::string l (line);
				std::string hash;
				GitLine *gitLine;

				l.pop_back();
				hash = parseLine(l);
				gitLine = new GitLine(log.getEntry(hash), l.size() -1 -hash.size());
				wasComment = gitLine->eval(l, wasComment, isPython);
				lines.push_back(gitLine);
				free(line);
				line = nullptr;
				len = 0;
			}
		if (line)
			free(line);
		fclose(f);
		::wait(&s);
	}
}

std::string GitFile::parseLine(const std::string &line)
{
	std::stringstream ss(line);
	std::string hash;

	ss >> hash;
	return hash;
}

const std::list<const GitLine *> &GitFile::getLines() const
{ return lines; }

