#include <iostream>
#include <utility>
#include <list>

#include "StatMaker.hpp"
#include "HtmlOutput.hpp"

bool sortPair(const std::pair<std::string, double> &a, const std::pair<std::string, double> &b)
{
	return a.second > b.second;
}

int StatMaker::printMap(const StatMaker::statEntry &e, HtmlOutput *out, int n)
{ return StatMaker::_printMap(e, false, n, out); }

int StatMaker::printRatioMap(const StatMaker::statEntry &e, HtmlOutput *out, int n)
{ return StatMaker::_printMap(e, true, n, out); }

void StatMaker::printPair(const std::pair<std::string, double> &pair, int bigger, bool isRatio, double total)
{
	std::cout.width(bigger +2);
	std::cout << pair.first;
	std::cout.fill(' ');
	std::cout << "\t";
	std::cout.width(10);
	std::cout << pair.second;
	if (!isRatio)
	{
		std::cout.fill(' ');
		if (total != 0)
			std::cout << "\t" << (pair.second *100)/ total << "%" << std::endl;
	}
	else
		std::cout << std::endl;
}

int StatMaker::_printMap(const StatMaker::statEntry &e, bool isRatio, int n, HtmlOutput *html)
{
	size_t bigger = 0;
	double total = 0;
	const int _n = n;
	std::list<std::pair<std::string, double> > items;
	std::pair<std::string, double> last;

	for (auto i = e.cbegin(); i != e.cend(); i++)
	{
		total += (*i).second;
		if ((*i).first.size() > bigger)
			bigger = (*i).first.size();
		items.push_back(std::make_pair((*i).first, (*i).second));
	}
	items.sort(sortPair);
	if (n == -1)
		n = items.size();
	if (isRatio)
		last = std::make_pair("Avg: ", total / e.size());
	else
		last = std::make_pair("Total: ", total);
	for (auto i = items.cbegin(); i != items.cend() && n > 0; i++ , n--)
		printPair(*i, bigger, isRatio, total);
	printPair(last, bigger, isRatio, total);
	std::cout << std::endl;
	if (html)
	{
		std::list<std::string> labels;
		int j = 0;
		for (auto i = items.begin(); i != items.end() && (++j <= _n || _n == -1); i++)
			labels.push_back((*i).first);
		html->addDataLabel(labels);
		j = 0;
		for (auto i = items.begin(); i != items.end() && (++j <= _n || _n == -1); i++)
		{
			double value = (*i).second;
			html->addData(value);
		}
		html->closeData();
	}
	return e.size();
}

int StatMaker::printTrue(const StatMaker::statEntry &e, HtmlOutput *html)
{
	size_t bigger = 0;
	int result =0;

	for (auto i = e.cbegin(); i != e.cend(); i++)
		if ((*i).first.size() > bigger && (*i).second >= 1)
			bigger = (*i).first.size();
	for (auto i = e.cbegin(); i != e.cend(); i++)
	{
		std::cout.width(bigger +2);
		std::cout << (*i).first;
		std::cout.fill(' ');
		std::cout << std::endl;
		html->addData((*i).first);
		result++;
	}
	std::cout << std::endl;
	return result;
}

StatMaker::statEntry StatMaker::merge(const statEntry &a, const statEntry &b)
{
	statEntry result;

	for (auto i = a.cbegin(); i != a.cend(); i++)
		result[(*i).first] = (*i).second;
	for (auto i = b.cbegin(); i != b.cend(); i++)
		result[(*i).first] = result[(*i).first] + (*i).second;

	return result;
}

