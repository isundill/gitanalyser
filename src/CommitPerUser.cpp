#include "GitLog.hpp"
#include "CommitPerUser.hpp"

StatMaker::statEntry CommitPerUser::compute(const GitLog &log, const std::list<GitEntity*>&) const
{
	const GitLog::LogMap &map = log.getMap();
	StatMaker::statEntry result;

	for (auto i = map.cbegin(); i != map.cend(); i++)
	{
		GitLog::LogEntry *entry = (*i).second;
		result[entry->email]++;
	}
	return result;
}

