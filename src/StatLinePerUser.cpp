#include "GitFile.hpp"
#include "StatLinePerUser.hpp"
#include "GitLog.hpp"

StatMaker::statEntry LinePerUser::compute(const GitLog &a, const std::list<GitEntity*>&l) const
{
	StatMaker::statEntry result;
	for (auto _l = l.cbegin(); _l != l.cend(); _l++)
	{
		const GitEntity *log = *_l;

		const GitDir *dir = dynamic_cast<const GitDir *> (log);
		const GitFile *file = dynamic_cast<const GitFile *> (log);

		if (file)
			for (auto i = file->getLines().cbegin(); i != file->getLines().cend(); i++)
			{
				const GitLog::LogEntry *entry = (*i)->getEntry();
				if (!entry)
					continue;
				result[entry->email] = result[entry->email] +1;
			}
		else if (dir)
		{
			StatMaker::statEntry c = this->compute(a, dir->getEntities());
			result = StatMaker::merge(result, c);
		}
	}
	return result;
}

