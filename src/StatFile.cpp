#include "GitFile.hpp"
#include "StatFile.hpp"
#include "GitLog.hpp"

#define MIN_TEXT ("Minimum lines")
#define MAX_TEXT ("Maximum lines")
#define AVG_TEXT ("Average lines")

StatMaker::statEntry StatFile::doCompute(const std::list<GitEntity*>&l) const
{
	StatMaker::statEntry result;
	bool first = true;

	for (auto _l = l.cbegin(); _l != l.cend(); _l++)
	{
		const GitEntity *log = *_l;

		const GitDir *dir = dynamic_cast<const GitDir *> (log);
		const GitFile *file = dynamic_cast<const GitFile *> (log);

		if (file)
		{
			size_t nbLines = file->getLines().size();

			if (nbLines == 0)
				continue;
			if (result[MAX_TEXT] < nbLines || first)
				result[MAX_TEXT] = nbLines;
			if ((result[MIN_TEXT] > nbLines || first))
				result[MIN_TEXT] = nbLines;
			result["total"] += nbLines;
			result["nbFiles"]++;
		}
		else if (dir)
		{
			StatMaker::statEntry c = this->doCompute(dir->getEntities());

			if (c.find(MAX_TEXT) != c.end() && (result[MAX_TEXT] < c[MAX_TEXT] || first))
				result[MAX_TEXT] = c[MAX_TEXT];
			if (c.find(MIN_TEXT) != c.end() && (result[MIN_TEXT] > c[MIN_TEXT] || first) && c[MIN_TEXT] != 0)
				result[MIN_TEXT] = c[MIN_TEXT];
			result["total"] += c["total"];
			result["nbFiles"] += c["nbFiles"];
		}
		first = false;
	}
	return result;
}

StatMaker::statEntry StatFile::compute(const GitLog &, const std::list<GitEntity *>&l) const
{
	StatMaker::statEntry result = this->doCompute(l);

	result[AVG_TEXT] = result["total"] / result["nbFiles"];
	result.erase("total");
	result.erase("nbFiles");
	return result;
}

