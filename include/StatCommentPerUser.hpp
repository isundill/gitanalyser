#pragma once

#include "StatMaker.hpp"

class CommentPerUser: public StatMaker
{
	public:
		virtual statEntry compute(const GitLog &, const std::list<GitEntity *> &root) const;
};

class CommentPerLine: public StatMaker
{
	public:
		virtual statEntry compute(const GitLog &, const std::list<GitEntity *> &root) const;

	private:
		virtual void doCompute(
				const std::list<GitEntity *> &root,
				std::map<std::string, unsigned long> &com,
				std::map<std::string, unsigned long> &line) const;
};

