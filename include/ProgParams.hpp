#pragma once

#include <list>
#include <string>

class Params
{
	public:
		typedef std::list<std::string> t_fileList;

	public:
		Params();
		Params(const std::list<std::string> &args);
		~Params();

		void readArgs(const std::list<std::string> &args);
		bool isExcluded(const std::string &path) const;
		bool htmlOutput() const;
		std::string getOutputFile() const;

		std::string rev() const;
		t_fileList path() const;

		static bool isRev(const std::string &);

	private:
		std::string _rev;
		t_fileList _path;
		std::list<std::string> excludeList;
		std::string html_output;

	private:
		bool isExcluded(const std::string &reg, const std::string &path) const;
};

