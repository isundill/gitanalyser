#pragma once

#include <map>
#include <string>

class GitLog
{
	public:
		struct LogEntry
		{
			std::string email;
			std::string message;
			struct tm time;
			unsigned int usefull;
			unsigned long long int totalChar;

			std::string toString(const std::string &hash) const;
		};
		typedef std::map<std::string, LogEntry *> LogMap;

	public:
		GitLog(const std::string &rev);
		~GitLog();

		LogEntry *getEntry(const std::string &hash);
		const LogMap &getMap()const;

	private:
		GitLog();
		void parseLine(const std::string &);

	private:
		LogMap map;
};

