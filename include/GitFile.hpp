#pragma once

#include <list>
#include <string>

#include "GitLog.hpp"
#include "GitLine.hpp"

class Params;

class GitEntity
{
	public:
		virtual ~GitEntity();

		virtual void debug(size_t level =0) const;
		virtual void doMagic(const Params &, GitLog &) =0;

	public:
		static GitEntity *create(const std::string &path, const Params &);

	protected:
		GitEntity(const std::string &path);

	private:
		GitEntity();

	protected:
		const std::string _filename;
};

class GitFile: public GitEntity
{
	public:
		GitFile(const std::string &path);
		~GitFile();

		void doMagic(const Params &, GitLog &);

		const std::list<const GitLine *> &getLines() const;

	private:
		std::string parseLine(const std::string &line);
		std::list<const GitLine *> lines;
};

class GitDir: public GitEntity
{
	public:
		GitDir(const std::string &path, const Params &);
		~GitDir();

		void debug(size_t) const;
		void doMagic(const Params &, GitLog &);

		const std::list<GitEntity *> &getEntities() const;

	private:
		std::list<GitEntity *> _content;
};

