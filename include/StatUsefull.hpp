#pragma once

#include "StatMaker.hpp"

class UsefullCommit: public StatMaker
{
	public:
		statEntry compute(const GitLog &, const std::list<GitEntity*> &root) const;
};

