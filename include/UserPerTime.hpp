#pragma once

#include "StatMaker.hpp"

class UserPerTime: public StatMaker
{
	public:
		UserPerTime(int from, int to);

		statEntry compute(const GitLog &, const std::list<GitEntity *> &root) const;

	private:
		UserPerTime();

	private:
		int from;
		int to;
};

