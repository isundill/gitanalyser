#pragma once

#include "StatMaker.hpp"

class CharPerLine: public StatMaker
{
	public:
		virtual statEntry compute(const GitLog &, const std::list<GitEntity *> &root) const;
};

