#pragma once

#include <map>
#include <string>
#include "ProgParams.hpp"

class GitLog;
class GitEntity;
class HtmlOutput;

class StatMaker
{
	public:
		typedef std::map<std::string, double> statEntry;

	public:
		virtual statEntry compute(const GitLog &, const std::list<GitEntity *> &root) const =0;

		static int printMap(const statEntry &, HtmlOutput *, int maxLine=-1);
		static int printRatioMap(const statEntry &, HtmlOutput *, int maxLine=-1);
		static int printTrue(const statEntry &, HtmlOutput *);
		static statEntry merge(const statEntry &, const statEntry &);

	private:
		static int _printMap(const statEntry &, bool isRatio, int maxLine, HtmlOutput *);
		static void printPair(const std::pair<std::string, double>&, int, bool, double);
};

