#pragma once

#include "StatMaker.hpp"

class CommitPerUser: public StatMaker
{
	public:
		statEntry compute(const GitLog &, const std::list<GitEntity *> &root) const;
};

