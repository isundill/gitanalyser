#pragma once

#include <fstream>
#include <string>
#include <list>

class HtmlOutput
{
	public:
		HtmlOutput(const std::string &dir);
		virtual ~HtmlOutput();

		void writeJs() const;
		void open();
		virtual void close();

		std::ofstream &operator()();

		virtual void startSection(const std::string &name,
				const std::string &type,
				const std::string &description ="");

		void addDataLabel(const std::list<std::string> &labels);
		template <typename T>
		void addData(const T &data);
		void closeData();

	protected:
		virtual void printHeader();
		virtual void printFooter();

	private:
		HtmlOutput();
		virtual void endSection();

		const std::string path;
		std::ofstream htmlOut;

		bool firstData;
		std::string currentType;
		std::string currentName;
};

