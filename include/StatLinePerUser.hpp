#pragma once

#include "StatMaker.hpp"

class LinePerUser: public StatMaker
{
	public:
		statEntry compute(const GitLog &, const std::list<GitEntity*> &root) const;
};

