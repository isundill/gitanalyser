#pragma once

#include <string>
#include "GitLog.hpp"

class GitLine
{
	public:
		GitLine(GitLog::LogEntry *commit, size_t len);
		~GitLine();
		bool eval(const std::string &line, bool wasComment, bool isPython); //return true if beginComment detected
		const GitLog::LogEntry *getEntry() const;
		bool haveComment() const;

	private:
		const GitLog::LogEntry *co;
		bool containComment;
};

