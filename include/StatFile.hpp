#pragma once

#include "StatMaker.hpp"

class StatFile: public StatMaker
{
	public:
		virtual statEntry compute(const GitLog &, const std::list<GitEntity *> &root) const;

	private:
		statEntry doCompute(const std::list<GitEntity *>&) const;
};

